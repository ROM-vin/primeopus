@extends('index')

@section('content')

@include('modal.modalService')  
    <div class="container-fluid" id="servicesBanner"></div>
    
    <div class="container">
        <h2 class="sectionHeader-h2 bannerHeader">Services</h2>    
    </div>

    <div class="container">
        <h1 class="text-uppercase headerColorServices" style="font-weight:bolder">REPAIRS</h1>
    </div>



    <div class="container p-2  my-4 servicesDesign">
    <h3 class="headerColorServices mt-2">Submersible Pumps</h3>

        <div class="row service-nav ">
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-v1.jpg')}}"  class="lazy servicesIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-2.jpg')}}" class="lazy servicesIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-3.jpg')}}" class="lazy servicesIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-4.jpg')}}" class="lazy servicesIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-5.jpg')}}"  class="lazy servicesIMG" alt="">
                </button>
            </div>

            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-8.jpg')}}" class="lazy servicesIMG " alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-9.jpg')}}" class="lazy servicesIMG " alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-10.jpg')}}" class="lazy servicesIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-11.jpg')}}"  class="lazy servicesIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-12.jpg')}}" class="lazy servicesIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service1">
                    <img data-src="{{asset('img/submersible-13.jpg')}}"  class="lazy servicesIMG" alt="">
                </button>
            </div>

        </div>
    </div>

    <div class="container p-2  my-4 servicesDesign">
    <h3 class="headerColorServices mt-2">Centrifugal Pumps</h3>

        <div class="row service-nav">
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service2">
                    <img data-src="{{asset('img/centrifugal-1.jpg')}}"  class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service2">
                    <img data-src="{{asset('img/centrifugal-2.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service2">
                    <img data-src="{{asset('img/centrifugal-3.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service2">
                    <img data-src="{{asset('img/centrifugal-4.jpg')}}"  class="lazy serviceIMG"  alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service2">
                    <img data-src="{{asset('img/centrifugal-5.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service2">
                    <img data-src="{{asset('img/centrifugal-6.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service2">
                    <img data-src="{{asset('img/centrifugal-7.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
        </div>
    </div>

    <div class="container p-2  my-4 servicesDesign">
        <h3 class="headerColorServices mt-2">Dewatering Pumps</h3>

        <div class="row service-nav">
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-1.jpg')}}"  class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-2.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-3.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-4.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-5.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-6.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-7.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-8.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-9.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-10.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-11.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service3">
                    <img data-src="{{asset('img/dewatering-12.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
        </div>
    </div>
    
    <div class="container p-2  my-4 servicesDesign">
        <h3 class="headerColorServices mt-2">Submersible Mixers</h3>

        <div class="row service-nav">

                    <div class="col-md-2 text-center" >
                        <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-1.jpg')}}"  class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-2.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-3.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center" >
                        <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-1.jpg')}}"  class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-2.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-3.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-4.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-5.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-6.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-4.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-5.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-6.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-7.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-8.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-9.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>  
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-7.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-8.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-2nd-9.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-10.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
                    <div class="col-md-2 text-center">
                        <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service4">
                            <img data-src="{{asset('img/mixer-1st-11.jpg')}}" class="lazy serviceIMG" alt="">
                        </button>
                    </div>
            
        </div>
    </div>

    <div class="container p-2  my-4 servicesDesign">
        <h3 class="headerColorServices mt-2">Progressive Cavity Pumps</h3>

        <div class="row service-nav">
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-1.jpg')}}"  class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-2.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-3.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-4.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-5.jpg')}}"  class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-6.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-7.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-8.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-9.jpg')}}"  class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-10.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service5">
                    <img data-src="{{asset('img/progressive-cavity-11.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
        </div>
    </div>

    <div class="container p-2  my-4 servicesDesign">
        <h3 class="headerColorServices mt-2">Rebonding of Tempering Belt</h3>

        <div class="row service-nav">
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service6">
                    <img data-src="{{asset('img/rebonding-1.jpg')}}"  class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service6">
                    <img data-src="{{asset('img/rebonding-3.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service6">
                    <img data-src="{{asset('img/rebonding-4.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service6">
                    <img data-src="{{asset('img/rebonding-5.jpg')}}"  class="lazy serviceIMG" alt="">
                </button>
            </div>
            <div class="col-md-2 text-center">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service6">
                    <img data-src="{{asset('img/rebonding-6.jpg')}}" class="lazy serviceIMG" alt="">
                </button>
            </div>
        </div>
    </div>

    <hr>

<div class="container p-2  my-4 servicesDesign">
    <h2 class="text-uppercase headerColorServices" style="font-weight:bolder">Site Equipment Check Up & Inspection</h2>
    <div class="row service-nav">
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup1.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup3.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup4.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup5.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup6.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup7.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup8.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup10.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup11.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup14.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service7">
                <img data-src="{{asset('img/checkup15.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

    </div>
</div>
<hr>
<div class="container p-2  my-4 servicesDesign">
    <h2 class="text-uppercase headerColorServices" style="font-weight:bolder">Testing & Commissioning</h2>
    <div class="row service-nav">
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service8">
                <img data-src="{{asset('img/commision1.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service8">
                <img data-src="{{asset('img/commision2.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service8">
                <img data-src="{{asset('img/commision3.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
  
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service8">
                <img data-src="{{asset('img/commision7.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service8">
                <img data-src="{{asset('img/commision9.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service8">
                <img data-src="{{asset('img/commision11.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service8">
                <img data-src="{{asset('img/commision12.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#service8">
                <img data-src="{{asset('img/commision14.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

    </div>
</div>
<hr>
<div class="container">
    <h2 class="text-uppercase headerColorServices" style="font-weight:bolder">Installation</h2>
</div>

<div class="container p-2  my-4 servicesDesign">
    <h3 class="text-capitalize headerColorServices mt-2">sulzer</h3>
    <div class="row service-nav">
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation3.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.1.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.2.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.3.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.4.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.5.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.6.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.7.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.8.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.9.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.10.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation2.12.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation3.2.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation3.3.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation5.2.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation5.6.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation1">
                <img data-src="{{asset('img/installation5.7.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
    </div>
</div>

<div class="container p-2  my-4 servicesDesign">
    <h3 class="text-capitalize headerColorServices mt-2">netzsch</h3>
    <div class="row service-nav">
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation3">
                <img data-src="{{asset('img/installation4.1.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation3">
                <img data-src="{{asset('img/installation4.2.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation3">
                <img data-src="{{asset('img/installation4.3.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation3">
                <img data-src="{{asset('img/installation4.4.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
    </div>
</div>

<div class="container p-2  my-4 servicesDesign">
    <h3 class="text-capitalize headerColorServices mt-2">flynn</h3>
    <div class="row service-nav">

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation2">
                <img data-src="{{asset('img/installation1.3.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation2">
                <img data-src="{{asset('img/installation1.4.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation2">
                <img data-src="{{asset('img/installation1.6.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>

        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation2">
                <img data-src="{{asset('img/installation1.8.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation2">
                <img data-src="{{asset('img/installation1.9.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation2">
                <img data-src="{{asset('img/installation1.10.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
        <div class="col-md-2 text-center">
            <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target="#installation2">
                <img data-src="{{asset('img/installation1.11.jpg')}}" class="lazy serviceIMG" alt="">
            </button>
        </div>
    </div>
</div>



<div class="contanier mx-3">
    <hr>
</div>
@endsection