<nav class="navbar navbar-expand-md navbar-light border-bottom fixed-top" id="mainNav" stlye="background-color:rgba(0,0,0,0.5)">
  <a class="navbar-brand" href="{{route('index')}}"><img class="" src="{{asset('img/prime-name1.png')}}" alt="Navbar" id="prime-name"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item mainNavLink">
            <a class="nav-link js-scroll-trigger" href="{{route('index')}}">home</a>
        </li>

        <li class="nav-item mainNavLink">
            <a class="nav-link js-scroll-trigger" id="Navproduct" href="#product">products</a>
        </li>
        
        <li class="nav-item mainNavLink">
            <a class="nav-link js-scroll-trigger" id="Navservices" href="#services">services & projects</a>
        </li>
        
        <li class="nav-item mainNavLink">
            <a class="nav-link js-scroll-trigger" id="Navexhibits"  href="#Exhibits">news & events</a>
        </li>
        
        <li class="nav-item mainNavLink">
            <a class="nav-link js-scroll-trigger" id="Navcareers" href="#careers">careers</a>
        </li>

        <li class="nav-item mainNavLink">
            <a class="nav-link js-scroll-trigger" id="Navcontact" href="#footer">contact us</a>
        </li>

        <li class="nav-item mainNavLink">
            <a class="nav-link js-scroll-trigger  disable-link" id="servicePage" >services</a>
        </li>

        <li class="nav-item mainNavLink">
            <a class="nav-link js-scroll-trigger disable-link" id="projectPage" >projects</a>
        </li>
    </ul>
   
  </div>
</nav>