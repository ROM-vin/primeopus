@extends('index')

@section('content')
    @include('content.carousel')
    @include('content.products')
    @include('content.exhibitions')
    @include('content.career')
    
@endsection