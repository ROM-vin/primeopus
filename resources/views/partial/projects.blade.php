@extends('index')

@section('content')

@include('modal.modalProject')
    <div class="container-fluid" id="servicesBanner"></div>
    <div class="container">
        <h2 class="sectionHeader-h2 bannerHeader">Projects</h2>
    </div>
       
    <div class="container p-2 servicesDesign">

        <div class="row">
            <div class="col-md-2 mobileView" >
                <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project1-modal-lg">
                    <img data-src="{{asset('img/project1.jpg')}}"  class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project2-modal-lg">
                    <img data-src="{{asset('img/project2.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project3-modal-lg">
                    <img data-src="{{asset('img/project3.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project4-modal-lg">
                    <img data-src="{{asset('img/project4.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project5-modal-lg">
                    <img data-src="{{asset('img/project5.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <!-- <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project6-modal-lg">
                    <img data-src="{{asset('img/project6.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div> -->
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project7-modal-lg">
                    <img data-src="{{asset('img/project7.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project8-modal-lg">
                    <img data-src="{{asset('img/project8.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project9-modal-lg">
                    <img data-src="{{asset('img/project9.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project10-modal-lg">
                    <img data-src="{{asset('img/project10.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project11-modal-lg">
                    <img data-src="{{asset('img/project11.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project12-modal-lg">
                    <img data-src="{{asset('img/project12.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project13-modal-lg">
                    <img data-src="{{asset('img/project13.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <!-- <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project14-modal-lg">
                    <img data-src="{{asset('img/project14.jpg')}}" class="w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project15-modal-lg">
                    <img data-src="{{asset('img/project15.jpg')}}" class="w-100" alt="">
                </button>
            </div> -->
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project16-modal-lg">
                    <img data-src="{{asset('img/project16.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
            <div class="col-md-2 mobileView">
                <button  type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".project17-modal-lg">
                    <img data-src="{{asset('img/project17.jpg')}}" class="lazy serviceIMG w-100" alt="">
                </button>
            </div>
        </div>
    </div>
    <div class="contanier mx-3">
        <hr>
    </div>
@endsection