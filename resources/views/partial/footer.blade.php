<div class="container">
    <div class="container-fluid" id="footer">
        <div class="row" id="footer">
            <div class="col-md-8 px-3 pt-4">
            <h2>Contact Us</h2>
                <div class="row">
                    <div class="col-md-8">
                        <div class="mapouter">
                            <div class="gmap_canvas">
                                <iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=Block%206%2C%20Lot%2026%2C%20Faith%20Street%2C%20St.%20Catherine%20Village%2C%20Barangay%20San%20Isidro%2C%20San%20Isidro%2C%20Paranaque%20City%2C%201700%20Metro%20Manila&t=&z=11&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="yes" marginheight="0" marginwidth="0"></iframe>
                            </div>
                        </div>
                        
                        <!-- <a href="https://www.google.com/maps/place/Prime+Opus,+Inc./@14.4737421,121.0139021,16z/data=!4m5!3m4!1s0x3397ce513d87226d:0x62aaf8776372cf1!8m2!3d14.4761714!4d121.0141005" style="text-decoration:none;color:black" target="_blank">
                            <img src="{{asset('img/prime-location.png')}}" class="border border-dark rounded" width="100%" alt="">
                            <sup>*click the map to open google map</sup>
                        </a> -->
                    </div>

                    <div class="col-md-4">
                            <h6><i class="fas fa-map-pin"></i>&nbsp;Location</h6>
                            <p class="pl-4">Block 6, Lot 26, Faith Street, St. Catherine Village, Barangay San Isidro, San Isidro, Paranaque City, 1700 Metro Manila</p>
                            <h6><i class="fas fa-phone"></i>&nbsp;Telephone No.</h6>
                            <p class="pl-4">(632) 8 820 14 21 <br> (632) 8 478 60 13 <br> (632) 8 995 31 44</p>
                            <h6><i class="fas fa-envelope"></i>&nbsp;E-mail</h6>
                            <p class="pl-4">info@primeopusinc.com</p>
                            <h6><i class="fab fa-facebook"></i>&nbsp;Facebook Page</h6>
                            <p class="pl-4"><a href="https://www.facebook.com/Primeopusinc" target="_blank">facebook.com/Primeopusinc</a></p>
                    </div>
                </div>
            </div>


            <div class="col-md-4 px-3 pt-4">

                <div class="pt-1 pl-5 border-left contactFormDesign">
                <h4>Send us a Message</h4>
                    <form>
                        @csrf
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="contactName" placeholder="Name">
                        </div>

                        <div class="form-group">
                            <input type="email" name="email" class="form-control" id="eMail" placeholder="E-mail">
                        </div>
                        
                        <div class="form-group">
                            <input type="email" name="number" class="form-control" id="contactNumber" placeholder="Contact Number">
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" name="message" id="Message" placeholder="Message" rows="5"></textarea>
                        </div>

                        <button type="button" class="btn btn-prime w-100" id="btnSend">Send</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="text-center mt-2 p-2">
        <span class="mb-0">&copy; Prime&nbsp;Opus&nbsp;Inc. <script>document.write(new Date().getFullYear())</script> | Powered by: <a href="https://naotech.com.ph/" target="_blank">Naotech Inc.</a></span>
        <!-- Developed by: Ervin Jay A. Orendain, Fernando Siapco -->
    </div>

    
</div>
