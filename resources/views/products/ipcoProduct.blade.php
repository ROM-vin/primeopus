
<div class="modal fade bd-Ipco-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="img/ipco.png" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0">IPCO provides advanced process solutions that require a deep knowledge of customer production processes across a wide range of industries and geographies. We offer bake oven belts, Rotoform granulation system and Rotary depositor Module (RDM). In a creative partnership with you, we will develop innovative solutions designed to pave the way to success.</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco1.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">bake oven belt</h4>
                        <p class="card-text">Bake oven belts have to be able to withstand extraordinary stresses. They are tensioned, tracked, heated up, cooled down, and turned around rollers thousands of times a day. And every single steel belt includes a welded or riveted joint that has to be just as capable of withstanding this treatment as the belt material itself.</p>
                        <p class="card-text">IPCO carbon steel belts are, as standard, delivered in a hardened and tempered condition and have well-rounded edges. If required practically any surface finish can be supplied. Perforated and embossed belts are also available.</p>
                    </div>
                </div>
                <!-- <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco2.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">carbon steel belt</h4>
                        <p class="card-text">IPCO carbon steel belts are, as standard, delivered in a hardened and tempered condition and have well-rounded edges. If required practically any surface finish can be supplied. Perforated and embossed belts are also available.</p>
                    </div>
                </div> -->
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco3.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">rotoformer</h4>
                        <p class="card-text">The Sandvik Rotoform process combines the Rotoformer drop depositor with a steel belt cooler to create a system capable of producing pastilles of highly uniform shape,stability and quality, in an environmentally friendly manner</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco4.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">stainless steel belt</h4>
                        <p class="card-text">IPCO stainless steel belts are, as standard, delivered in cold rolled (mill finish) condition with a surface roughness of Ra < 0.40 mm, ground finish with a surface roughness in the range Ra = 0.10 to Ra = 1.5mm and have well-rounded edges</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco5.jpg" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">PMX 400</h4>
                        <h5>Batch Coloring & Flavoring Unit is one such system.</h5>
                        <p class="card-text">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul>
                                        <li>Standalone system.</li>
                                        <li>Batch sizes from 500 –15 000 kg.</li>
                                        <li>Easy operation via 19” touchscreen</li>
                                        <li>Recipe controlled</li>
                                        <li>Easy to clean – offline hot water cleaning</li>
                                        <li>Inline mixing</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul>
                                        <li>Variable speed and capacity</li>
                                        <li>Lost and weight premix injection control</li>
                                        <li>High flexibility: solid, powder or paste additives</li>
                                        <li>No temper and piping contamination</li>
                                        <li>Online monitoring and control of premix injection</li>
                                    </ul>
                                </div>
                            </div>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco6.jpg" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">RDM 1500</h4>
                        <h5>Rotary Depositing Module is one such system</h5>
                        <p class="card-text">Unique modular design
                            <div class="row">
                                <div class="col-md-6">
                                    <ul>
                                        <li>Retrofit for plastic belt cooling tunnel lines</li>
                                        <li>Easy cleaning</li>
                                        <li>Flexible/quick changeover &lt;30 min</li>
                                        <li>Servo motor driven</li>
                                        <li>Minimal tooling parts</li>
                                    </ul>
                                </div> 
                                <div class="col-md-6">
                                    <ul>
                                        <li>High capacity in small chip counts</li>
                                        <li>Low maintenance costs</li>
                                        <li>Built-in utilities</li>
                                        <li>Easy operation via 12” touchscreen</li>
                                        <li>Recipe controlled HMI</li>
                                    </ul>
                                </div>
                            </div>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco7.jpg" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">SPL 1200 </h4>
                        <h5>Specialty Line is one such system</h5>
                        <p class="card-text">
                            <div class="row">
                                <div class="col-md-6">
                                    <ul>
                                        <li>Highly flexible production</li>
                                        <li>Wide product range</li>
                                        <li>Capacity range 300–900 kg/hour</li>
                                        <li>Triple pass steel belt cooling</li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul>
                                        <li>Flexible/quick changeover &lt;30 min</li>
                                        <li>Modular tooling</li>
                                        <li>High OEE and efficiency</li>
                                        <li>Industrial, gourmet & decorative products</li>
                                    </ul>
                                </div>
                            </div>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco8.jpg" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">TPE 1500</h4>
                        <h5>Triple Pass Plastic-Belt Moulding Line is one such system</h5>
                        <div class="row card-text">
                            <div class="col-md-6">
                                <ul>
                                    <li>Triple pass plastic belt cooling tunnel</li>
                                    <li>High output 700–2 000 kg/hour</li>
                                    <li>Flexible/quick changeover &lt;30 min</li>
                                    <li>Servo cutter</li>
                                    <li>Modular set-up (40 ft container shipment)</li>
                                    <li>Pre-assembled unit</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <li>On site installation in one week</li>
                                    <li>High OEE and efficiency</li>
                                    <li>Low maintenance costs</li>
                                    <li>Fast, easy cleaning</li>
                                    <li>High flexibility with rapid changeover</li>
                                    <li>Centralised product and utility connections</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine1.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Block Moulding Line (BML)</h4>
                        <p class="card-text">
                            A large volume servo piston depositor feeds pre-heated flex moulds that are weight checked before being transported into the cooling tunnel. This can be either ‘walk-through’ in design for maximum accessibility, or a narrow, space-saving design for installations where footprint is critical.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine2.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize"> Chip & Chunk Moulding Line (CCM)</h4>
                        <p class="card-text">
                            This innovative moulding line is based on the Rotary Depositor Module, an extremely reliable unit with low maintenance requirements. The module features an exchangeable outer shell enabling production of chips in sizes from 30,000-300 pcs/kg.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine3.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize"> Multi Decoration Line (MDL)</h4>
                        <p class="card-text">
                            "As well as medium capacity throughput of industrial chips and chunks in a range of sizes, it also enables the production of decorative and gourmet chocolate products such as:
                           <div class="row">
                                <div class="col-md-5">
                                    <ul>
                                        <li>Rolls</li>
                                        <li>Blossoms</li>
                                        <li>Shavings</li>
                                        <li>Spiral rolls</li>
                                    </ul>
                                </div>
                                <div class="col-md-5">
                                    <ul>
                                        <li>Multi-colour rolls</li>
                                        <li>Plaquettes</li>
                                        <li>Grids</li>    
                                    </ul>
                                </div>
                           </div>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine4.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize"> Multi-Layer Chocolate Line (MCC)</h4>
                        <p class="card-text">
                            Combining three production lines in a single unit delivers a forming capacity of between 4-6 tonnes per hour – depending on chip size. It also allows the production of different recipes or product shapes/sizes at the same time.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine5.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize"> Rotary Depositor Module (RDM)</h4>
                        <p class="card-text">
                            Suitable for the production of chips from 30,000-300 pcs/kg, this versatile module delivers a consistently sized product at throughput rates of up to 2000 kg/hour. The outer depositor shell can be removed and replaced to enable production of chips of a different size, with changeover in less than 30 minutes.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine6.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize"> Speciality Product Line (SPL)</h4>
                        <p class="card-text">
                            As well as medium capacity throughput of industrial chips and chunks in a range of sizes, it also enables the production of decorative and gourmet chocolate products such as:
                            <div class="row">
                                <div class="col">
                                    <ul>
                                        <li>Bossoms</li>
                                        <li>Shavings</li>
                                    </ul>
                                </div>
                            </div>
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine7.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Triple Pass Economic (TPE)</h4>
                        <p class="card-text">
                            This triple pass cooling enables high throughput rates from a system with a footprint significantly less than would otherwise be the case. Chips of different sizes can be produced by exchanging the depositor outer shell of the RDM or the plate on the GDM.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine8.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">ROTOFORM HP (High Performance) </h4>
                        <p class="card-text">
                            The Rotoform HP (High Performance) rotary depositing system is built on the proven strengths of our Rotoform 4G but brings particular benefits to the challenge of processing of high viscosity products at high volumes.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine9.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Rotoform HS (High Speed)</h4>
                        <p class="card-text">
                            The Rotoform HS (High Speed) granulator builds on the same basic principle as the standard 4G models but features a much larger rotating shell that enables significantly higher capacity production.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine10.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Rotoform MI (Mini)</h4>
                        <p class="card-text">
                            Delivering all the benefits of our standard Rotoform system but on a very small scale, the Rotoform MI (Mini) is ideally suited to use in laboratory testing operations and within existing plants to define quality, production rates and other key parameters of products in the development stage. The Rotoform MI granulation system is based on a small scale steel belt cooler and matching Rotoform feeding device, consisting of a stator, metering bar, rotating shell, refeed bar and the drive.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-machine11.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">ROTOFORM S8</h4>
                        <p class="card-text">
                            The Rotoform S8 is the latest addition to our ranges and has been specifically developed for low cost sulphur pastillation, while still delivering the quality of end product has been central to the success of the Rotoform principle for well over 30 years.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-accesory1.png" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">COMPACT BELT TRACKING</h4>
                        <p class="card-text">
                            With this active tracking device it is possible to handle situations when the running conditions on the conveyor are not constant, for example, when the product is loaded onto the belt differently from time to time, temperature changes or uneven temperature.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-accesory5.jpg" class="lazy card-img-top rounded-lg w-75" alt="image not found">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">GRAPHITE STATION</h4>
                        <p class="card-text">
                            To ensure the best working conditions of the conveyor at high temperature such as in ovens and presses, solid steel belts require graphite to be deposited the inside of the belt at regular intervals
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-accesory2.png" class="lazy card-img-top rounded-lg w-75" alt="image not found">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">SHAFTS</h4>
                        <p class="card-text">
                            Standard shafts which are used together with sheaves in a steel belt conveyor are available for belt widths up to 1500 mm. Shafts are dimensioned for a belt tension of 7 MPa and max pulling force corresponding to 15 MPa belt tension.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-accesory3.png" class="lazy card-img-top rounded-lg w-75" alt="image not found">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">SHEAVES</h4>
                        <p class="card-text">
                            Sheaves are mounted on standard shafts in both drive and tension ends. With sheaves is it also possible to track steel belts with a true-tracking rope (V-rope). The tension in the belt is limited by the sheaves to 7 MPa and the pulling force must not exceed 2 500 N for grooved sheaves and 1 250 N for other rim designs.
                        </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/ipco-accesory4.png" class="lazy card-img-top rounded-lg w-75" alt="image not found">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">SOLID BELTS SUPPORT, BARS</h4>
                        <p class="card-text">
                            In most cases steel belts in ovens (baking, frying etc.) are supported by skid bars of special cast-iron, placed transversally underneath the belt. To ensure the best working conditions it is recommended that min 10% of the skids should be graphite bars.
                        </p>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>