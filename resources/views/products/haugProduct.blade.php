
<div class="modal fade bd-Haug-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="img/HAUG-Logo.png" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0">Haug Quality Equipment is the leading supplier of leak detector, leak testers and quality assurance equipment for the food packaging industry.</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/HAUG.jpg" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Pack-Vac Leak Detector</h4>
                        <p class="card-text">Simple, reliable, repeatable leak detection. Continual testing on the production line will detect sealing problems before they snowball. A quality control system that incorporates the Pack-Vac Leak Detector will allow you to quickly and reliably setup packaging lines. Materials can be tested for seal integrity during package development. Altitude simulation assures package integrity during trucking and airfreight. Above all, the Pack-Vac Leak Detector will catch defects before they get to your customers.</p>
                    </div>
                </div>
                <hr>
                <div class="row mb-3">
                    <div class="mx-auto">
                        <h4>CHOOSING THE RIGHT PACK-VAC LEAK DETECTOR</h4>
                    </div>
                </div>
                <div class="row px-5">
                    <div class="col-md-6 text-justify px-5">
                        <h5>CONNECTION REQUIREMENTS</h5>
                        <p>Our Pack-Vac Leak Detectors generate vacuum in one of two ways depending on available utilities. The Venturi Vacuum Generator operates using compressed air. We recommend this set-up if standard dry plant air (60-80 P.S.I., 2-6 C.F.M.) is available. Ideal for abusive production environments. If compressed air is not available, we recommend the Electric Vacuum Pump. Our electric model comes equipped with a quiet and durable vacuum pump, 115 VAC standard (unless otherwise specified).</p>
                    </div>
                    <div class="col-md-6 text-justify px-5">
                        <h5>SIZE</h5>
                        <p>Our Pack-Vac Leak Detectors are available in eight production models. Haug also can craft custom sizes, so there is always one to meet your needs. Not sure what size you need? Send us your package and we will conduct tests to determine the correct Pack-Vac Leak Detector for your application. Haug Quality Equipment is now producing the largest acrylic vacuum chamber style leak detectors on the market, with interior dimensions of 32″ x 26″ x 20″. With our new machining center, we can produce tanks even larger than that.</p>
                    </div>
                </div>
                <div class="row mx-5">
                    <table class="table text-center mx-5">
                        <thead>
                            <tr>
                                <th scope="col">MODEL #</th>
                                <th scope="col">TANK INSIDE DIMENSIONS</th>
                                <th scope="col">POWERED BY</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>PVCA 110808</td>
                                <td>11″ x 8″ x 8″</td>
                                <td>Compressed air</td>
                            </tr>
                            <tr>
                                <td>PVCA 181210</td>
                                <td>18″ x 12″ x 10″</td>
                                <td>Compressed air</td>
                            </tr>
                            <tr>
                                <td>PVEL 181210</td>
                                <td>18″ x 12″ x 10″</td>
                                <td>Electric</td>
                            </tr>
                            <tr>
                                <td>PVCA 201413</td>
                                <td>20″ x 14″ x 13″</td>
                                <td>Compressed air</td>
                            </tr>
                            <tr>
                                <td>PVEL 201413</td>
                                <td>20″ x 14″ x 13″</td>
                                <td>Electric</td>
                            </tr>
                            <tr>
                                <td>PVCA 242015</td>
                                <td>24″ x 20″ x 15″</td>
                                <td>Compressed air</td>
                            </tr>
                            <tr>
                                <td>PVCA 302016</td>
                                <td>30″ x 20″ x 16″</td>
                                <td>Compressed air</td>
                            </tr>
                            <tr>
                                <td>PVCA 322620</td>
                                <td>32″ x 26″ x 20″</td>
                                <td>Compressed air</td>
                            </tr>
                            <tr>
                                <td>PVCA 342620</td>
                                <td>34″ x 26″ x 20″</td>
                                <td>Compressed air</td>
                            </tr>
                            <tr>
                                <td>PVCA XXXXXX</td>
                                <td>Custom</td>
                                <td>Compressed air</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>