<div class="modal fade bd-Kongskilde-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="img/kongskilde.png" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0">Kongskilde, the company’s core businesses consist of GRAIN (grain handling equipment) and INDUSTRY (system solutions for pneumatic conveying and handling of process material in the plastic, paper and packaging industries).</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/kongskilde3.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">multiAir blower</h4>
                        <p class="card-text">The MultiAir® high pressure blower is the power source in many Kongskilde solutions and can be found in installations all over the world</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/kongskilde2.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">TLR Blower</h4>
                        <p class="card-text">The Kongskilde TRL high-pressure blowers are available in a wide range of sizes–providing a variety of performance characteristics capable of meeting specific industrial requirements.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/kongskilde4.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">multicutter</h4>
                        <p class="card-text">The Multicutter consists of a fixed knife and a rotating set of blades driven by a direct-coupled motor</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/kongskilde5.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">rotary valve</h4>
                        <p class="card-text">The Kongskilde Rotary Valves, type RF, are suitable for feeding of granulates, (granules, pellets, flakes, small moulded products etc.) either into a pressurized pneumatic conveying line or as a metering feed to a subsequent process.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/kongskilde1.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">aspirator</h4>
                        <p class="card-text">The Kongskilde KIA12, KIA20 and KIA60 Aspirator are designed to separate light impurities or dust from re-processed material or granulated plastic.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/kongskilde6.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">separator</h4>
                        <p class="card-text">The Kongskilde STS separator is designed for pneumatic conveying systems in the paper, plastic and packaging industries</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/kongskilde7.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">venturi system</h4>
                        <p class="card-text">The Kongskilde Inline Venturi system is very suitable for the conveying of continuous edge trim and off-cuts.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/kongskilde8.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">suction blower</h4>
                        <p class="card-text">The electrically powered Kongskilde suction blowers SUC offer one of the most versatile conveying systems on the market for free-flowing types of granulate.</p>
                        <p class="card-text">The high-performance suction blowers, combined with the simple and flexible OK pipe system, will fit in anywhere, irrespective of building facilities.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto">
                        <img data-src="img/kongskilde1.1.jpg" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Penumatic Conveying System</h4>
                        <p class="card-text  text-justify">Granules conveying with rotary valve<br>
                        The rotary valve system is suitable for high and low capacity installations. The rotary valve feeds the material into the positive air stream generated by the Multiair blower. After having been carried through the smooth pipe system, the cyclone separates the material from the air stream at the discharge point. These systems can be built quickly using Kongskilde’s unique standard modular components and can be installed virtually anywhere.<br>
                        Kongskilde offers a wide range of standard components to build totally customized systems with conveying distances up to 200m.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto">
                        <img data-src="img/kongskilde1.2.jpg" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Trim Handling System</h4>
                        <p class="card-text  text-justify">Cut trim with RVS separator system<br>Using the Kongskilde MultiSeparator RVS 75 enables the removal of small sized waste material by suction from production lines involved in the manufacture of paper, plastic foil, aluminum foil and packaging material. The material is downsized by the MultiCutter. The RVS 75 separates light material from the suction line air stream and re-introduces it into the pressure line. The RVS 75 MultiSeparator is particularly suitable for installations where a strong suction effect is essential. The conveyed material can be directed to different destinations through pipe diverters to keep different types of material separated.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto">
                        <img data-src="img/kongskilde1.3.jpg" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Trim Handling System</h4>
                        <p class="card-text  text-justify">Cut trim with RVS separator system<br>Using the Kongskilde MultiSeparator RVS 75 enables the removal of small sized waste material by suction from production lines involved in the manufacture of paper, plastic foil, aluminum foil and packaging material. The material is downsized by the MultiCutter. The RVS 75 separates light material from the suction line air stream and re-introduces it into the pressure line. The RVS 75 MultiSeparator is particularly suitable for installations where a strong suction effect is essential. The conveyed material can be directed to different destinations through pipe diverters to keep different types of material separated.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto">
                        <img data-src="img/kongskilde1.4.jpg" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Shredded material separator</h4>
                        <p class="card-text  text-justify">Shredded material with KS/RVS system<br>For larger systems with more shredders that demand more suction power, a Kongskilde KS/RVS system is the ideal solution for conveying the shredded material. This system can be sized to evacuate shredded material from more shredders through a pipe system to bring the material to one destination, such as a container with compactor.<br>This allows for installation of the shredders on locations that are more convenient and efficient for handling and moving the material to be downsized in the shredder without having troubles or needing to take into consideration how the shredded material is going to be delivered.<br>No space is required in the working area for storage and transportation of the shredded material, and the working environment is kept clean.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto">
                        <img data-src="img/kongskilde1.5.jpg" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Granules Conveying with Suction System</h4>
                        <p class="card-text  text-justify">Granules conveying with suction system<br>The vacuum conveying system is ideally suited for conveying from various collecting points to a single destination. The material can be moved horizontally and vertically. The high-performance Kongskilde blowers, combined with the simple and flexible Kongskilde OK pipe system will fit virtually anywhere, irrespective of building facilities.<br>Ideally suited for evacuating granulators, containers or other production machines via vacuum and then releasing the material by gravity vertically through a rotary valve.<br>The vacuum air is then discharged or routed through a dust filtration system.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>