<div class="modal fade bd-Wirebelt-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="{{asset('img/wirebelt.png')}}" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0 text-justify">Wire Belt Company offers many styles and specifications of conveyor belts to suit your specific application and improve your process efficiencies. We provide conveyor belts for industries such as Food Processing, Textiles, Electronics, Agriculture, Automotive, and more. Our first class Customer Service and global presence means we can provide fast and simple distribution around the world</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.21.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Straight Conveyors </h4>
                        <p>Wire Belt offers a range of straight conveyors designed to ensure efficient operation, improved belt life and maximum cleaning ability.  Our straight conveyors feature an open construction with the belt being supported on food grade high density blue polyethylene wear strips clipped to the stainless steel framework.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.19.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Flex-Turn Conveyors</h4>
                        <p>Field-proven and dependable Flex-Turn® units gently convey products in process around corners while maintaining proper product alignment. They offer the tightest level transfer and smallest inside turning radius available with an open mesh belt. Smooth, non-collapsing product handling minimizes potential damage to delicate products. The compact design solves tight production line space problems.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.20.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Ladder-Flex Conveyor</h4>
                        <p>The necessity to spread, separate or converge products while they are moving on a process belt conveyor is a major requirement for a wide range of food, confectionery and industrial applications.  In many cases this allows processors to run narrower conveyors saving factory floor space.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.1.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Flat Flex/ Flat Flex XT</h4>
                        <p>Flat-Flex® belts promote maximum flow through and are the proven solution for major processors.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                </ul>
                            </div>

                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Coating</li>
                                    <li>Breading</li>
                                    <li>Drainage</li>
                                    <li>Freezing</li>
                                    <li>Enrobing</li>
                                    
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Frying</li>
                                    <li>Baking</li>
                                    <li>Dough Rolling</li>
                                    <li>Battering</li>
                                </ul>
                            </div>
                        </div>
                        

                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.2.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Eye Flex</h4>
                        <p>Established & trusted design keeps your process on the move.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                </ul>
                            </div>

                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Drainage</li>
                                    <li>Freezing</li>
                                    <li>Frying</li>
                                    <li>Baking</li>
                                    <li>Washing</li>
                                </ul>
                            </div>

                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.3.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Compact Grid</h4>
                        <p>Compact-Grid™ stainless steel conveyor belt is engineered to offer superior support for small products.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                    <li>Coating</li>
                                    <li>Freezing</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Frying</li>
                                    <li>Baking</li>
                                    <li>Elevating</li>
                                    <li>De-Elevatin</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.4.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Balanced Spiral</h4>
                        <p>Benefits of Balanced Spiral belt include straight-running operation, an excellent strength to weight ratio and an extremely wide variety of mesh specifications to suit each individual application.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Cooling</li>
                                    <li>Coating</li>
                                    <li>Drainage</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Baking</li>
                                    <li>Baking</li>
                                    <li>Industrial Curtains</li>
                                    <li>Annealing</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.5.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Versa-link Belt</h4>
                        <p>Versa-Link™ stainless steel conveyor belt makes installing your conveyor belt quick and easy</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                    <li>Coating</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Drainage</li>
                                    <li>Freezing</li>
                                    <li>Enrobing</li>
                                    <li>Frying</li>
                                    <li>Baking</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.6.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Flat Spiral</h4>
                        <p>Flat Spiral belting is often found in baking and washing applications where small apertures are required alongside a flat conveying surface.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Drainage</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Baking</li>
                                    <li>Washing</li>
                                    <li>Annealing</li>
                                    <li>Curing</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.7.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Rolled Baking Oven</h4>
                        <p>Honeycomb is an ideal choice for any application which requires both durability and an open belt design whilst maintaining a flat carrying surface.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Freezing</li>
                                    <li>Blanching</li>
                                    <li>Dewatering</li>
                                    <li>Cooling</li>
                                    <li>Quenching</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Pasteurising</li>
                                    <li>Washing</li>
                                    <li>Degreasing</li>
                                    <li>Drying</li>
                                    <li>Industrial Ovens</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Heavy Duty</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.8.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Honeycomb/Honey Comb Duplex</h4>
                        <p>Flat Spiral belting is often found in baking and washing applications where small apertures are required alongside a flat conveying surface.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Drainage</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Baking</li>
                                    <li>Washing</li>
                                    <li>Annealing</li>
                                    <li>Curing</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.9.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Cordweave Belt</h4>
                        <p>Cordweave belts offer an extremely close and flat mesh for applications where very small items are being conveyed.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Baking</li>
                                    <li>Annealing</li>
                                    <li>Elevating</li>
                                    <li>De-Elevating</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.10.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Ladder Conveyor Belt</h4>
                        <p>Ladder Belting is a simple but effective style of conveyor belt, commonly found in bakeries.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Drainage</li>
                                    <li>Freezing</li>
                                    <li>Washing</li>
                                    <li>Inspection</li>
                                    <li>Packaging</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Elevating</li>
                                    <li>De-Elevating</li>
                                    <li>Turning</li>
                                    <li>Filling</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.11.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Filter belt</h4>
                        <p>Wire Belt offer continuous food fryer filter belts suitable for a range of proprietary food fryer filter conveyors.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-5">
                                <ul class="m-0 pb-0">
                                    <li>Filtering</li>
                                    <li>Swarf Removal</li>
                                    <li>Elevating</li>
                                    <li>De-Elevating</li>
                                    <li>Proofing</li>
                                </ul>
                            </div>
                            <div class="col-2">
                                <ul class="m-0 pb-0">
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.12.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Self Tracking Belt</h4>
                        <p>Self-Tracking Belt is a very versatile type of belting.</p>
                        <p>This type of belting is used in a wide variety of industries, including food and pharmaceuticals, chemicals, plastics and polymers.</p>
                        <p>Process applications that can be accomplished include washing, draining, drying, curing, cooking and freezing.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                    <li>Drainage</li>
                                </ul>
                            </div>
                            <div class="col-5">
                                <ul class="m-0 pb-0">
                                    <li>Frying</li>
                                    <li>Baking</li>
                                    <li>Curing</li>
                                    <li>Elevating</li>
                                    <li>De-Elevating</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.13.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Plate Link</h4>
                        <p>Wire Belt Company’s Plate Link belt features an extremely robust design and high load capacity, making it the perfect choice for arduous engineering processes such as presswork, die-casting and forging.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Drainage</li>
                                    <li>Sorting</li>
                                    <li>Elevating</li>
                                </ul>
                            </div>
                            <div class="col-5">
                                <ul class="m-0 pb-0">
                                    <li>De-Elevating</li>
                                    <li>Waste Treatment</li>
                                    <li>Blanching</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.14.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Flexible Rod Belt</h4>
                        <p>Flexible Rod belts are designed primarily for multi-tier spiral conveyors commonly used in the food industry. With the ability to side flex, the belt can also be used for conveyors arranged to go around obstacles.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                </ul>
                            </div>
                            <div class="col-5">
                                <ul class="m-0 pb-0">
                                    <li>Drainage</li>
                                    <li>Freezing</li>
                                    <li>Elevating</li>
                                    <li>De-Elevating</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.15.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Chain Link</h4>
                        <p>Wire Belt Company’s Chain Link belting is the simplest available wire belt design, suitable for light duty use in drying and cooling applications.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-5">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooling</li>
                                    <li>Drainage</li>
                                    <li>Industrial Curtains</li>
                                </ul>
                            </div>
                            <div class="col-5">
                                <ul class="m-0 pb-0">
                                    <li>Lift Guards</li>
                                    <li>Elevating</li>
                                    <li>De-Elevating</li>
                                    <li>Industrial Furnaces</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.16.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Flex Turn Conveyor Belt</h4>
                        <p>Flex-Turn® conveyor belts gently convey products in process around corners while maintaining proper product alignment and spacing.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Transport</li>
                                    <li>Cooking</li>
                                    <li>Heating</li>
                                    <li>Drying</li>
                                    <li>Cooling</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Coating</li>
                                    <li>Breading</li>
                                    <li>Drainage</li>
                                    <li>Freezing</li>
                                    <li>Enrobing</li>
                                </ul>
                            </div>
                            <div class="col-3">
                                <ul class="m-0 pb-0">
                                    <li>Frying</li>
                                    <li>Baking</li>
                                    <li>Dough Rolling</li>
                                    <li>Battering</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.17.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Specialty Conveyor Belt</h4>
                        <p>When you have a complex product or process that requires special handling such as: moving product up steep inclines or down sharp declines, maintaining product separation, minimizing points of contact, or keeping the product aligned in rows.</p>
                        <p>Typical Applications:</p>
                        <div class="row justify-content-center">
                            <div class="col-6">
                                <ul class="m-0 pb-0">
                                    <li>Automotive brake pads</li>
                                    <li>Electronic circuit boards</li>
                                    <li>Chocolates</li>
                                   
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul class="m-0 pb-0">
                                    <li>Bagels</li>
                                    <li>Snack foods</li>
                                    <li>Many other applications</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt1.18.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">TC-327 Belt</h4>
                        <p>Wire Belt's TC-327™ tortilla cooling conveyor belt is designed with 74% open surface area, which allows for maximum airflow on tortilla cooling lines. TC-327™ is made of stainless steel, which helps to prevent product sticking from moisture</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12 mb-3 text-center">
                        <h3 class="font-weight-bold">Components</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt-accessory1.1.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Drive Component</h4>
                        <p>Wire Belt manufactures a wide range of drive sprockets.  Wire Belt standard sprockets are designed specifically to drive Flat-Flex belts smoothly and efficiently.  Sprockets not specifically designed for Flat-Flex should not be used as they may cause the belt to surge, jump teeth and result in premature failure.</p>
                        
                            <p class="font-weight-bold">Wire Belt drive component advantages:</p>
                            <ul>
                                <li>Precision machined to fit your exact belt</li>
                                <li>Manufactured to optimise your belt performance</li>
                                <li>Minimises unnecessary wear & tear on your belt like generic sprockets can</li>
                                <li>Designed to increase belt life, saving you money</li>
                                <li>Available in stainless steel, polyacetal plastic or PEEK (PolyEtherEther-Ketone).</li>
                            </ul>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt-accessory1.2.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">PEEK Components</h4>
                        <p>Wire Belt introduces our line of PEEK drive components. PEEK is an abbreviation for PolyEtherEther-Ketone, a high performance engineering thermoplastic that can operate at high temperatures and is less abrasive on your stainless steel belts than metal drive components. PEEK can be used continuously to 250°C and in hot water or steam without permanent loss in physical properties.</p>    
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt-accessory1.3.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Clean-Sweep™ Sprockets</h4>
                        <p>Wire Belt's innovative line of Clean-Sweep™ sprockets are specifically designed to deflect the amount of product buildup accumulated on your conveyor’s drive. This means that there is less of a chance for product loss & carryout, belt skipping due to product buildup, and belt breakage due to incorrect contact with the sprocket teeth. Clean-Sweep™ sprocket’s tooth chamfer is machined to reduce drive friction and can lengthen belt life.</p>    
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-12 mb-3 text-center">
                        <h3 class="font-weight-bold">Accessory</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt-accessory1.4.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">EZ-Splice® Joining Strand</h4>
                        <p>"Using EZ-Splice® belt joining strand during installation will dramatically extend your belt life! Belt installations that are rushed and improperly made are often the cause of belt breakage and downtime. EZ-Splice® is a pre-formed, pre-bent joining strand that requires no bending or weaving during installation. This helps to prevent any weak spots in the belt joint."</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt-accessory1.5.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Belt Joining Clips</h4>
                        <p>Belt clips are used for joining the belt during installation and for making fast minor repairs to the belt. They are available in one space and three space units. </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt-accessory1.6.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Belt Joining Tubes</h4>
                        <p>"Joining tubes can be used to create a hybrid join between a full strand weave and the use of joining clips. A single strand is cut into smaller single space sections, which are woven into the belt and joined together by crimping a stainless steel joining tube onto the adjacent wire sections. This method maintains the belt strength of a full strand weave whilst eliminating the distortion that occurs during the weaving process."</p>    
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/wirebelt-accessory1.7.jpg')}}" class="lazy card-img-top rounded-lg w-75" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Belt Maintenance Tools</h4>
                        <p>"Wire Belt Company offers a range of belt maintenance tools for easy removal, repair or installation of metal conveyor belts.  All tools are packaged in a reusable storage sleeve to help maintain the tools precision and cleanliness."</p>    
                    </div>
                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>