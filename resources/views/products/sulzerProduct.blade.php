<div class="modal fade bd-Sulzer-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="{{asset('img/sulzer.png')}}" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0 text-justify">Sulzer is a global leader in fluid engineering. They specialize in pumping, agitation, mixing, separation and application technologies for fluids of all types. Sulzer ensures that all the location fulfill its high quality and safety standard. The company is constantly investing in state-of-the-art machine tools, packaging and test facilities.</p>
                        </div>
                    </div>
                </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer10.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Submersible pumps</h4>
                        <p class="card-text">The submersible sewage pumps type ABS XFP are designed for wet or dry installation in pumping stations. The XFP pumps use Premium Efficiency IE3 motors to offers significant energy savings, along with excelent rag handling, long-term reliability and a future-proof design.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer2.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Dewatering pumps</h4>
                        <p class="card-text">Our dewatering pumps are designed to meet tough challenges in construction sites, tunnels and mines. The compact and robust design combimes low weight with high capacity and high abrasion resistance for efficient handling of dirty water.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer6.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Submersible Mixers</h4>
                        <p class="card-text">These standard mixers have an integral motor and are suitable for agitating, blending, mixing, dissolving and suspension of solids in municipal treatment plants, industry and agriculture. Sulzer offers multiple and gear driven mixers with either standard or explosion-proof motor enclosures. A wide range of brackets and adapters facilitate easy upgrades of existing installations.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer3.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Disc Diffusers</h4>
                        <p class="card-text">Disc diffuser system type ABS offers a number of alternative membrane and porous aeration diffuser models that are easy to install and maintain. Special features that improve the operation reliability and efficiency include the non-return valve, available on all models and the sliding ring, available on the types ABS PIK 300 abd PRK 300. High oxygen transfer efficiency combined with low pressure drops makes the diffusers extremely efficetve.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer8.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Sucoflow</h4>
                        <p class="card-text">The robust Sucoflow disk diffuser has an EPDM membrane perforated using a specially developed process. The large effective surface area and the  thread means it is a good choice for stainless steel piping and liftable systems. THe membrane is reliably mounted on the frame with a stainless steel wire. A built-in non-return valve provides additional safety for planned or unplanned outages.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer9.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Turbo Compressors</h4>
                        <p class="card-text">The new generation of world class high efficiency turbocompressors, HST 20, 30 and 40, gives exceptional energy savings form wire to air, savings in maintenance costs, stable eficiency with magnetic bearings, compact and cost effective installation and an optimized process by an intuitive compressor control. </p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer5.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Submersible Aerators</h4>
                        <p class="card-text">These products are suitable for wastewater treatments in municipal and industrial plants. Application areas include mixing, equalization and activated sludge tanks. Suitable also for SBR-reactors and sludge storage tanks at a water depth between 2 and 9 m. The aerator is free-standing on the bottom of the basin and hence can be installed without emptying the basin. The self aspirating aerator has a very low noice level due to it's efficient silencer. Compared to conventional surface aerators the submerged XTA aerator creates no aerosol thus preventing formation of coliform bacteria.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer7.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Submersible Axial Flow Pumps</h4>
                        <p class="card-text">Save space and reduce installation costs with the AFLX range of submersible axial-flow pumps, designed for direct installation in compact rising mains. Available with Premium Efficiency IE3 motors. Featuring highly efficient three- to five-blade mixed flow impellers. The AFLX-pumps ensure high reliability and efficiency.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer4.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Scaba - Top mounted Agitator</h4>
                        <p class="card-text">Scaba top-mounted vertucal agutators are used for mixing and agitating process liquids in demanding wastewater treatment applications. They ensure homogenous mixing results, high process reliability, high efficiency, low costs and low environmental stress.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/sulzer1.png')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">AGISTAR - Side mounted agitator</h4>
                        <p class="card-text">AGISTAR SSA side-mounted horizontal agitators are ideal for mixing and agitating process liquids in demanding wastewater treatment applications. They ensure homogeneous mixing results, process reliability, high effiency, low operating costs and low environmental stress.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
