<div class="modal fade bd-Scanbelt-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img data-src="{{asset('img/scanbelt.png')}}" class="lazy modal-title w-75" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0">Scanbelt is the largest and most flexible program of modular plastic conveyor belts and in the process from design and tool making to plastic moulding and assembling of whole belts.</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/scanbelt3.jpg')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.12</h4>
                        <p class="card-text">A true MINIPITCH with a 12,5 mm pitch. Designed for light transport. It is able to go around a 14 mm nose bar for very tight transfers. Placement of sprockets is flexible across the belt width, i.e. no risk of assembly or installation mistakes. The links are designed with open rod access and are consequently very easy to clean. They can be delivered with flights and rubber top surface. The rod retention system is protected by the RCD.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/scanbelt4.jpg')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.25</h4>
                        <p class="card-text">The S.25 is a range of 25 mm pitch belts designed to solve all aspects of light to medium transport. The series consists of 20 belt types with distinct characteristic s when it comes to open area and surface structure. Closed and open belts with smooth or structure surface. Raised ribs and rubber surface. Belts with very good cleanability for meat and poultry industry. The rod retention system is protected by the RCD.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/scanbelt5.jpg')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.50</h4>
                        <p class="card-text">The S.50 series is a range of 50 mm belts designed for medium to heavy transport. The series consists of more than 20 types of belts with different surface structures and openings as well as a complete range of easy to clean belts for high hygiene applications. Rubber surface and different nub top structures for good product grip. The series contains a vast span of pulling forces from light cooling belts to heavy-duty belts for car and pallet handling.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/scanbelt6.jpg')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.75</h4>
                        <p class="card-text">The S. 75 series is a range of 75 mm pitch belts designed for super-heavy transport. The very high pulling force now makes it possible to make longer conveyors than before. It is available with both smooth and non-skid surface as well as accessories such as integrated sideguards and comb flights</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/scanbelt1.jpg')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Radius Belt</h4>
                        <p class="card-text">25 years in spiral and radius belt business has resulted in a range of 25 and 50 mm pitch belts going in J-curves and S-curves. The belts have a market low collapse factor (from 1,5:1 x belt width) without compromising the pulling force and product placement. The reinforced outer links gives a very high pulling force without any steel-to-steel contact.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="{{asset('img/scanbelt2.jpg')}}" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.06</h4>
                        <p class="card-text">A true MICROPITCH with a pitch of 6,25 mm and is thus the smallest pitch in the market. Designed for light transport in the bakery and seafood industry for cooling/ freezing applications as well as dairy production. It is able to go around a 7 mm nose bar for very tight transfers. The belt is “pinless” (i.e. without rods) and is consequently very easy to clean</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <img data-src="{{asset('img/S.12-401.jpg')}}" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.12-401</h4>
                        <img data-src="{{asset('img/s12-401desc1.jpg')}}" class="lazy rounded-lg w-100">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <img data-src="{{asset('img/S.12-406.jpg')}}" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.12-406</h4>
                        <img data-src="{{asset('img/s12-406desc1.jpg')}}" class="lazy rounded-lg w-100">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <img data-src="{{asset('img/S.12-408.jpg')}}" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.12-408</h4>
                        <img data-src="{{asset('img/s12-408desc1.jpg')}}" class="lazy rounded-lg w-100">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <img data-src="{{asset('img/S.12-408F.jpg')}}" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.12-408F</h4>
                        <p class="card-text">Friction top surface. Structure pattern, extremely good grip.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <img data-src="{{asset('img/S.12-438.jpg')}}" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.12-438</h4>
                        <img data-src="{{asset('img/s12-438desc1.jpg')}}" class="lazy rounded-lg w-100">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <img data-src="{{asset('img/S.12-448.jpg')}}" class="lazy card-img-top rounded-lg w-100" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">S.12-448</h4>
                        <img data-src="{{asset('img/s12-448desc1.jpg')}}" class="lazy rounded-lg w-100">
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <div class="row">
                            <h4 class="card-title font-weight-bold text-capitalize">accessories</h4>
                        </div>
                        <div class="row text-center">
                            <div class="col-md-3">
                                  <img data-src="{{asset('img/acc1.jpg')}}" class="lazy rounded-lg w-75" alt="">
                            </div>
                            <div class="col-md-3">
                                  <img data-src="{{asset('img/acc2.jpg')}}" class="lazy rounded-lg w-75" alt="">
                            </div>
                            <div class="col-md-3">
                                  <img data-src="{{asset('img/acc3.jpg')}}" class="lazy rounded-lg w-75" alt="">
                            </div>
                            <div class="col-md-3">
                                  <img data-src="{{asset('img/acc4.jpg')}}" class="lazy rounded-lg w-75" alt="">
                            </div>
                             
                        </div>
                        <br>
                        <div class="row text-center">
                            <div class="col-md-3">
                                <a href="http://scanbeltnordic.com/products-1/scanbelt_nordic/bearings_and_bearing_units.aspx" target="_blank">
                                    <img data-src="{{asset('img/acc5.jpg')}}" class="lazy rounded-lg w-75" alt="">
                                    <p class="text-uppercase">bearings & bearing units</p>
                                </a>  
                            </div>
                            <div class="col-md-3">
                                <a href="http://scanbeltnordic.com/products-1/scanbelt_nordic/chains_and_sprockets.aspx" target="_blank">
                                    <img data-src="{{asset('img/acc6.jpg')}}" class="lazy rounded-lg w-75" alt="">
                                    <p class="text-uppercase">chains & sprockets</p>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="http://scanbeltnordic.com/products-1/scanbelt_nordic/profiles.aspx" target="_blank">
                                    <img data-src="{{asset('img/acc7.jpg')}}" class="lazy rounded-lg w-75" alt="">
                                    <p class="text-uppercase">profiles</p>
                                </a>
                            </div>
                            <div class="col-md-3">
                                <a href="http://scanbeltnordic.com/products-1/scanbelt_nordic/accessories.aspx" target="_blank">
                                    <img data-src="{{asset('img/acc8.jpg')}}" class="lazy rounded-lg w-75" alt="">
                                    <p class="text-uppercase">accessories</p>
                                </a>
                            </div>
                             
                        </div>    
                    </div>
                </div>   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
