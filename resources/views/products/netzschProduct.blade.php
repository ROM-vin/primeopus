<div class="modal fade bd-Netzsch-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="img/netzsch.png" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0">Netzsch is an international technology company with headquarters in Germany. The Business Units Analyzing & Testing, Grinding & Dispersing and Pumps & Systems represent customized solutions at the highest level.</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/netzsch11.v2.png" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>
                    <div class="col-md-8 my-auto">
                        <h4 class="card-title font-weight-bold text-capitalize">PC Pump</h4>
                        <p class="card-text">NEMO progressing cavity pumps are used in all sectors of industries to convey almost all types of media continuosly, smoothly with low pulsation and dosing in proportion to speed.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/netzsch4.v2.png" class="my-auto lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8 my-auto">
                        <h4 class="card-title font-weight-bold text-capitalize">BT Immersible Pump</h4>
                        <p class="card-text">NEMO Immersible Pumps are used to empty barrels, containers, tanks, sedimentation tanks, pits etc. and where space is restricted, there is a risk of cavitation or there is very low NPSH. The pumps are also used to empty containers holding materials that are hazardous to water or the environment for which standard emptying via a flange on the bottom of the container is not permitted.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/netzsch6.png" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">FSIP PC Pump</h4>
                        <p class="card-text">The FSIP desgn is fully compatible to the existing BY and SY series. The concept consists of three stages, FSIP.ready, FSIP.advanced and FSIP.pro, which are designed to upgrade already installed pumps step by step, or which are available for new installed accroding to the individaul needs of our customers.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/netzsch7.png" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Hygienic Pump</h4>
                        <p class="card-text">The pumps are designed and manufactured in accordance with EHEDG and QHD guidelines, are CIP/SIP-capable and comply with the US 3-A Sanitary Standards. Two Rotor/stator geometries are available to ensure optimum performance.</p>
                        <p class="card-text">These pumps are suited for hygienic applications in the foodstuff, pharmaceutical, cosmetic and biotechnology industries for low and highly viscous media with and without solids.</p>
                    </div>
                </div> 
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/netzsch1.jpg" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">BF optional mit aBP-Module pump</h4>
                        <p class="card-text">The NEMO® BF is used in all branches of industry to provide continuous, pressure-stable, gentle and low-pulsation conveyance with dosing in proportion to rotation speed. It is employed primarily for highly viscous, compacted and crumbly substances that do not have a tendency to bridge.</p>
                        <p class="card-text">For optimal feed into the conveying elements, the pump housing is designed with an enlarged, rectangular hopper and conical force-feed chamber as well as a special coupling rod with a patented, horizontally positioned feeding screw.</p>
                        <p class="card-text">In block construction with a flanged drive, this pump is particularly compact and economical.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/netzsch2.png" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">BO Nemo Pump</h4>
                         <p class="card-text">Universally applicable – the NEMO® BO/BS hopper-Shaped Progressing Cavity Pump</p>
                         <p class="card-text">The NEMO® BO/BS Progressing Cavity Pump with square/rectangular hopper and coupling rod with feeding screw and force-feed chamber is employed in almost all branches of industry to provide continuous, pressure-stable, gentle and low-pulsation conveyance. This special design guarantees better feed into the conveying elements. Another great advantage of the NEMO® Progressing Cavity Pump is its ability to dose nearly any substance in proportion to rotation speed. In block construction with a flanged drive, this pump is particularly compact and economical.</p>
                    </div>
                </div>
                <hr>
                <!-- <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/netzsch3.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">BT Immersible pump 1</h4>
                         <p class="card-text">NEMO® Immersible Pumps are used to empty barrels, containers, tanks, sedimentation tanks, pits etc. and where space is restricted, there is a risk of cavitation or there is very low NPSH. The pumps are also used to empty containers holding materials that are hazardous to water or the environment for which standard emptying via a flange on the bottom of the container is not permitted.</p>
                    </div>
                </div>
                <hr> -->
                <div class="row">
                <div class="col-md-4 text-center">
                        <img data-src="img/netzsch5.png" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Dosing Pump</h4>
                         <p class="card-text my-auto">The NEMO® C.Pro Dosing Pump is a fully synthetic pump for the precise dosing of a great variety of chemicals in environmental technology and the chemical industry. Its synthetic construction makes it perfectly suited for the gentle and low-pulsation conveyance and dosing of aggressive or non-aggressive chemicals.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                <div class="col-md-4 text-center">
                        <img data-src="img/netzsch8.png" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Nemo BMax Pump</h4>
                         <p class="card-text">The NEMO® B.Max is setting new standards with its maximum mixing and conveying capabilities for biosubstrates. It is the result of continued development work on the NETZSCH NEMO® Progressing Cavity Pump with hopper and conveying screw.</p>
                         <p class="card-text">The pump housing has a large rectangular hopper and removable conical force-feed chamber, as well as a coupling rod with patented, horizontally positioned conveying screw, that guarantee optimal product feeding to the conveying elements. The positioning of the conveying supports on the hopper housing allows for maximum mixing of the substrate.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                <div class="col-md-4 text-center">
                        <img data-src="img/netzsch9.png" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Nemo BP Pump</h4>
                         <p class="card-text">For highly viscous, compacted and crumbly substances that have a tendency to bridge, the NEMO® BP Progressing Cavity Pump is the optimal solution. The NEMO® BP can be used in nearly all branches of industry to provide continuous, pressure-stable, gentle and low-pulsation conveyance with dosing in proportion to rotation speed. The special features of this model include the patented, horizontally positioned feeding screw, the integrated bridge breaker and the enlarged rectangular hopper. Optimal feed into the conveying elements is guaranteed by means of the coupling rod with this feeding screw and the conical force-feed chamber. The bridge breaker prevents bridging and makes the mixing of aggregates possible. The hopper dimensions can be adjusted to the individual application case. The block construction of the NEMO® BP with flanged drive is particularly compact and economical.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                            <img data-src="img/netzsch10.png" class="lazy card-img-top rounded-lg w-75 my-auto" alt="...">
                    </div>  
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Mini Dosing Hygienic Pump</h4>
                         <p class="card-text">The NEMO® Mini BH Hygienic Pump is employed above all in the foodstuff, pharmaceutical, cosmetic and chemical/biochemical industries for hygienic applications. It is ideal in these settings due in part to its continuous, pressure-stable, smooth and low-pulsation conveyance and in part to its capability of dosing in proportion to speed. In the block construction with flanged drive, the NEMO® Mini BH is particularly compact and economical.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                            <img data-src="img/netzsch12.png" class="lazy card-img-top rounded-lg w-50 my-auto" alt="...">
                    </div>  
                    <div class="col-md-8 my-auto">
                        <h4 class="card-title font-weight-bold text-capitalize">NEMO® Barrel Emptying Pumps</h4>
                         <p class="card-text">NEMO® Barrel Emptying Pumps section themselves automatically to the barrel or container to achieve almost residue-free emptying in the chemical, pharmaceutical and foods industries. The heart of the Barrel Emptying System is a volumetrically conveying NEMO® Progressing Cavity Pump.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                            <img data-src="img/netzsch13.png" class="lazy card-img-top rounded-lg w-50 my-auto" alt="...">
                    </div>  
                    <div class="col-md-8 my-auto">
                        <h4 class="card-title font-weight-bold text-capitalize">TORNADO®</h4>
                         <p class="card-text">NETZSCH TORNADO® self-priming, valveless, positive displacement pumps can be optimally customized to meet specific process and application requirements. They can be used for almost any media on intermittent, continuous or dosing applications.</p>
                         <p>TORNADO® pumps are particularly service and maintenance friendly; all parts that come into contact with the media are directly accessible without dismantling the pipework or disconnecting the drive. Their advantages are small space requirements due to their compact design, high performance and maximized operational reliability, and the physical separation between the pump head and bearing housing.</p>
                    </div>
                </div>                
            </div>            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>