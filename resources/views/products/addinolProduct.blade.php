<div class="modal fade bd-Addinol-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="img/ADDINOL-Logo.png" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0 text-justify">High performance lubricants from ADDINOL carry the "Made in Germany" seal of quality. Whether lubricants for industry or automotive sector - at the company's site in Leuna, they developed and produced at our in house laboratory. Commercial vehicles, gears, engines or turbines are operated globally with ADDINOL lubricating oils, lubricating greases, pastes or sprays and their use is supported by our on-sites expert. Convince yourself of our quality and expertise as an experienced oil manufacturer.</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol1.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Chain Lubricants</h4>
                        <p class="card-text">Conveyor chains, conveyor rollers, drive belts, continuous ovens press lines.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol2.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Compressor oils</h4>
                        <p class="card-text">Screw, rotary and reciprocating compressors.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol3.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Corrosion Protection Oils</h4>
                        <p class="card-text">Storage of components and tools, transport of finished products.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol4.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Food and Veverage Industry</h4>
                        <p class="card-text">Food grade lubricants for gearboxes, chains and hydraulics in food and pharmaceutical industries</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol5.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Gas Engine Oils</h4>
                        <p class="card-text">Combined heat and power plants, biogas plants.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol6.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Gear Oils</h4>
                        <p class="card-text">Steel industry, mining rails, wind turbines</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol7.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Greases</h4>
                        <p class="card-text">Greasing of wheel bearings, window regulators and sunroofs on the car, use in industrial installations on slideways and rolling bearings</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol8.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Guideway Oils</h4>
                        <p class="card-text">Horizontal and vertical slideways and guideways.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol9.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Heat-Transfer Oils</h4>
                        <p class="card-text">Wood processing, film production, paper industry.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol10.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Oil for Metal Works</h4>
                        <p class="card-text">Machining and non-cutting forming of ferrous and non-ferrous materials</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol11.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Textile Machine Oil</h4>
                        <p class="card-text">Lubrication of needles and blanks in circular and flat knitting machines, general use in all stages of textile production</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/addinol12.png" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold pt-2">Turbine Oil</h4>
                        <p class="card-text">Use in water, gas and steam turbines, turbocompressors and transmission compressors</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="http://addinol.oilfinder.net/oilfilter_group7_catn0.html" target="_blank" class="mr-auto">http://addinol.oilfinder.net/oilfilter_group7_catn0.html</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>