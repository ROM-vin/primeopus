<div class="modal fade bd-Flynn-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto text-center">
                            <img data-src="img/flynn.png" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8">
                            <p class="mb-0 text-justify">Flynn with over 80 years of experience in combustion, both in the dry foods baking and flame treatment industry, they remain at the very cutting edge technology. Supplying a simple gasket or complete combustion control solutions, they are unique in supplying your every combustion need.</p>
                        </div>
                    </div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 my-auto text-center text-center">
                        <img data-src="img/flynn1.1.png" class="lazy card-img-top rounded-lg w-75" alt="Burner_Pipe_Distributor">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Burner Pipe Distributor 7 Distinct Flame Patterns</h4>
                        <p>The Flynn Distributor Pipe burner provides lateral flame adjustment to equalize product color and moisture across the band.</p>
                        <ul>
                            <li>Available in 2" Pipe</li>
                            <li>Single Point Adjustment</li>
                            <li>Interchangeable with existing pipe burners</li>
                            <li>Suitable for use with natural gas, propane and butane</li>
                        </ul>   
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.2.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Extruder Band Burner</h4>
                        <p>The EXTRUded Band Burner Assembly, with a larger cross section and smooth walls, is designed to minimize pressure drops offering superior uniformity, higher capacity, and good turn-down.</p>
                        <ul>
                            <li>Turn down to 200 BTU/inch of flame space.</li>
                            <li>Interchangeable with existing cast iron band burners </li>
                            <li>Suitable for use with natural gas, propane and butane</li>
                        </ul>   
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.3.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Extruder Band Distributor Burner</h4>
                        <p>The Flynn Selective Distributor burner provides lateral flame adjustment to equalize product color and moisture across the band. This Burner, with a larger cross section and smooth walls, is designed to minimize pressure drops offering superior uniformity, higher capacity, and good turn-down.</p>
                        <ul>
                            <li>Single Point Adjustment.</li>
                            <li>Interchangeable with existing cast iron band burners</li>
                            <li>Suitable for use with natural gas, propane and butane</li>
                        </ul>   
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.4.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">Flame Distributor Pipe Burner</h4>
                        <p>The Flynn Distributor Pipe burner provides lateral flame adjustment to equalize product color and moisture across the band.</p>
                        <ul>
                            <li>Single Point Adjustment.</li>
                            <li>Interchangeable with existing pipe burners</li>
                            <li>Suitable for use with natural gas, propane and butane.</li>
                        </ul>   
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.5.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">InfraRed Profile Burner</h4>
                        <ul>
                            <li>High Thermal Efficiency</li>
                                <ul>
                                    <li>Higher combustion efficiency with homogeneous premixing</li>
                                    <li>Lower pollutant(CO, NOx) with less excess air</li>
                                    <li>Lower CO2 Emission</li>
                                </ul>
                            <li>Short Flame</li>
                                <ul>
                                    <li>Enable compact design with a slim combustion chamber</li>
                                </ul>
                            <li>Fast Heat-up & Cool-down</li>
                                <ul>
                                    <li>Reached target temperature with 5 sec and cool downed within 1sec</li>
                                </ul>
                        </ul>
                        <p>Flynn Rating for woven IR material:</p>
                        <p>85,000 BTU per square foot – 24.89 KW per square foot<br>590.3 BTU per square inch – 172.85 W per square inch</p>   
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.6.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8">
                        <h4 class="font-weight-bold">InfraRed Profile Distributor Burner</h4>
                        <ul>
                            <li>High Thermal Efficiency</li>
                                <ul>
                                    <li>Higher combustion efficiency with homogeneous premixing</li>
                                    <li>Lower pollutant(CO, NOx) with less excess air</li>
                                    <li>Lower CO2 Emission</li>
                                </ul>
                            <li>Short Flame</li>
                                <ul>
                                    <li>Enable compact design with a slim combustion chamber</li>
                                </ul>
                            <li>Fast Heat-up & Cool-down</li>
                                <ul>
                                    <li>Reached target temperature with 5 sec and cool downed within 1sec</li>
                                </ul>
                        </ul>
                        <p>Flynn Rating for woven IR material:</p>
                        <p>85,000 BTU per square foot – 24.89 KW per square foot<br>590.3 BTU per square inch – 172.85 W per square inch</p>   
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.7.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>        
                    <div class="col-md-8">
                        <h4 class="font-weight-bold">InfraRed Profile Distributor Burner</h4>
                        <div class="container">
                            <div class="row">
                                <div class="<col mx-auto">
                                    <ul>
                                        <li>Patent Pending Technology</li>
                                        <li>Rigid Profile Design</li>
                                        <li>Available in Various Lengths</li>
                                        <li>Stainless Steel Construction</li>
                                        <li>Lateral Power is Very Even</li>
                                    </ul>
                                </div>
                                <div class="col mx-auto">
                                    <ul>
                                        <li>Extremely LO NOx</li>
                                        <li>Ideal for Product Development</li>
                                        <li>Enhanced Heat Transfer as Required in Process</li>
                                        <li>Simple Change Over Between IR and Blue Flame</li>
                                        <li>Fits in Standard Oven Sleeve without Modification</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <p>Flynn Rating for woven IR material:</p>
                        <p>85,000 BTU per square foot – 24.89 KW per square foot<br>590.3 BTU per square inch – 172.85 W per square inch</p>
                    </div>                
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.8.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8 my-auto">
                        <h4 class="font-weight-bold">Pipe Burner</h4>
                        <p>Flynn Pipe Burners are widely used for baking breads, cookies, crackers, snacks etc… These burners provide exceptional flame retention and uniformity at both low and high fire. Pipe burners can be manufactured in any length and in diameters of 1″ – 3″. The ribbons are constructed of stainless steel and can be configured in many different patterns to achieve the desired capacity.</p>
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.9.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8 my-auto">
                        <h4 class="font-weight-bold">Tri-Zone Adjustable Burner – 1-1/2” & 2”</h4>
                        <ul>
                            <li>BTU rating 80,000 BTU nominal for 1½” pipe</li>
                            <li>BTU rating 160,000 BTU nominal for 2″ pipe</li>
                            <li>Individual near, intermediate and opposite adjustment</li>
                            <li>Direct replacement for existing similar styles</li>
                            <li>Suitable for use with natural gas, propane and butane</li>
                        </ul>
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.10.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8 my-auto">
                        <h4 class="font-weight-bold">Uniglo InfraRed Burner</h4>
                        <p>Infrared heating is a gas burning technique in which premixed gas and air burns on a surface layer of a permeable and/or perforated medium.<br>Unlike segmented ceramic infrared burner designs, the Uniglo burner is a single piece construction, which provides a more even distribution of radiant heat over the entire burner surface.</p>
                        <ul>
                            <p>Maintenance</p>
                            <li>The Uniglo burner is essentially maintenance free.</li>
                            <li>Resistance to Shock</li>
                            <li>The Uniglo burner resists thermal and mechanical shocks without loss of burner efficiency</li>
                            <li>Radiant Output</li>
                            <li>Can be fired up to 85,000 btu/sq ft /hr</li>
                        </ul>
                        <div class="container">
                            <p>Applications:</p>
                            <div class="row">
                                <div class="<col mx-auto">
                                    <ul>
                                        <li>Bakery Ovens</li>
                                        <li>Snack-food Ovens</li>
                                        <li>Thermoforming</li>
                                    </ul>
                                </div>
                                <div class="col mx-auto">
                                    <ul>
                                        <li>Paper Drying</li>
                                        <li>Textile Drying</li>
                                        <li>Drying and Curing of Coatings</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.11.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8 my-auto">
                        <h4 class="font-weight-bold">Wafer Oven Burner</h4>
                        <p>The Extruded wafer burner is manufactured in 24″ flanged sections. The sections can be bolted together to attain any length burner for a particular oven. Any ribbon pattern can be used to assure the best results in heating the plates.</p>
                    </div>   
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 my-auto text-center">
                        <img data-src="img/flynn1.12.png" class="lazy card-img-top rounded-lg w-75" alt="Infra_Red-Ribbon">
                    </div>

                    <div class="col-md-8 my-auto">
                        <h4 class="font-weight-bold">Zig Zag Pipe Burner</h4>
                        <p>The ZigZag burner was designed for small Tortilla ovens where heating of a solid steel band in a continuous manner is crucial. These are used in ovens that produce approximately 50 to 100 Tortillas per minute.</p>
                    </div>   
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>