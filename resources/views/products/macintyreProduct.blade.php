
<div class="modal fade bd-Macintyre-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 my-auto">
                            <img data-src="img/macintyre1.png" class="lazy modal-title w-100" id="exampleModalLabel" alt="">
                        </div>
                        <div class="col-md-8 my-auto">
                            <p class="mb-0">MacIntyre Chocolate Systems is a worldwide supplier of chocolate manufacturing and processing equipment.</p>
                        </div>
                    </div>
                </div>
                   
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/macintyre1.3.jpg" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">refiner conche</h4>
                         <p class="card-text">The Refiner/Conche has a specially designed shaft equipped with grinding bars and pressure adjustment mechanism to adjust the pressure on the product between the grinding bars and lining bars (around the cylinder of the Refiner/Conche). This allows a reliable process to achieve and maintain a constant quality of the product from batch to batch.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4 text-center">
                        <img data-src="img/macintyre1.1.jpg" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Coating Pan</h4>
                         <p class="card-text">Coating Pan is designed to carry out coating operations of different types of tablets, pellets and granules.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                <div class="col-md-4 text-center">
                        <img data-src="img/macintyre1.4.jpg" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">roasting plant</h4>
                         <p class="card-text">The roasting of whole beans is the traditional cocoa roasting method and is preferred by numerous chocolate manufacturers due to the good aroma development.</p>
                    </div>
                </div>
                <hr>
                <div class="row">
                <div class="col-md-4 text-center">
                        <img data-src="img/macintyre1.2.jpg" class="lazy card-img-top rounded-lg w-50" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 class="card-title font-weight-bold text-capitalize">Lentil Roller Moulding Line</h4>
                         <p class="card-text">The Macintyre chocolate lentil roller machine operates on the principle of rotating two rollers (upon which, the two halves of the required product have been profiled) against one another, through which suitably tempered chocolate, or other at based masses, is passed to form solid units of the required shape.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
