@extends('index')

@section('content')
<div class="container-fluid"style="height:200px;background-image: url('/img/1.jpg');background-position:center center; background-size:cover;background-attachment:fixed;"></div>
<div class="container-fluid border-bottom">

    <div class="row">
        <div class="col-md-9">
            <div class="container my-1">
                <h3 class="sectionHeader-h3 pt-3" id="Exhibits">Afex Asia Food Expo 2019</h3>
                <div class="row px-5">
                  
                        <div class="slider-for w-100" id="exhibitMain">
                            <div><img data-src="{{asset('img/expo1.jpg')}}" class="lazy  p-2" alt="test"></div>
                            <div><img data-src="{{asset('img/expo2.jpg')}}" class="lazy  p-2" alt="test"></div>
                            <div><img data-src="{{asset('img/expo3.jpg')}}" class="lazy  p-2" alt="test"></div>
                            <div><img data-src="{{asset('img/expo4.jpg')}}" class="lazy  p-2" alt="test"></div>
                            <div><img data-src="{{asset('img/expo5.jpg')}}" class="lazy  p-2" alt="test"></div>
                            <div><img data-src="{{asset('img/expo6.jpg')}}" class="lazy  p-2" alt="test"></div>
                        </div>    
                  
                </div>

                <div class="row mx-5">
                    <div class="slider-nav w-100 mx-auto" id="exhibitSub">
                        <div><img data-src="{{asset('img/expo1.jpg')}}" class="lazy w-75 p-2" alt="test"></div>
                        <div><img data-src="{{asset('img/expo2.jpg')}}" class="lazy w-75 p-2" alt="test"></div>
                        <div><img data-src="{{asset('img/expo3.jpg')}}" class="lazy w-75 p-2" alt="test"></div>
                        <div><img data-src="{{asset('img/expo4.jpg')}}" class="lazy w-75 p-2" alt="test"></div>
                        <div><img data-src="{{asset('img/expo5.jpg')}}" class="lazy w-75 p-2" alt="test"></div>
                        <div><img data-src="{{asset('img/expo6.jpg')}}" class="lazy w-75 p-2" alt="test"></div>
                    </div>
                </div>
           
            </div>
        </div>

        <div class="col-md-3 border-left pt-5">
            <h3>Other News Posted</h3>
            <ul>
                <li>
                    <a href="{{route('afex2019')}}">Afex Asia Food Expo 2019</a>
                </li>
            </ul>
        </div>
    </div>
</div>


@endsection