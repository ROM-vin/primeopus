<!-- career -->
<div class="modal fade" id="career-Sales-Engineer" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content px-4 pt-1">
            <div class="modal-header">
                <h4 class="modal-title">Sales Engineer</h4>
                <button type="button" class="close" data-dismiss="modal" arial-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class=text-bold>Job Description</h3>
                <h5>Main Responsibilities:</h5>
                <ul>
                    <li>Promote company's products to new and existing clients.</li>
                    <li>Generate and execute sales plans in assigned competitively-held accounts.</li>
                    <li>Conduct technical presentation of products to clients, submit proposal, follow-up quotations and secure purchase order.</li>
                    <li>Provide monthly sales forecast and ensure that monthly/ yearly sales target is achieved.</li>
                    <li>Develop strong relationship with key decision makers in current and prospective customers, including plant or facility executives.</li>
                    <li>Organize clients visits/schedules</li>
                    <li>Address client's concerns on their enquiries and deliveries, and make sure to meet customer's needs.</li>
                    <li>Provide high standard of service/technical support to customers; identifying and resolving customer's challenges.</li>
                </ul>
                <h5>Job Type / Category</h5>
                <p style="text-indent:20px">Industry: Distributor/Heavy Industrial/Machinery/Equipment</p>
                <p style="text-indent:20px">Job Type: Full-time (Direct Hiring)</p>
                <p style="text-indent:20px">Work Schedule: Mondays - Fridays, 9:00 AM - 6:00 PM</p>

                <h5>Key Qualifications:</h5>
                <ul>
                    <li>Candidate must have at least Bachelor's Degree preferably in Engineering.</li>
                    <li>At least 1 year working experience in the related field is required for this position.</li>
                    <li>Working knowledge of industrial plant operations is an advantage.</li>
                    <li>Strong communication and leadership skills.</li>
                    <li>Proficient in MS Office applications.</li>
                    <li>Professional License is an advantage.</li>
                    <li>Driving skill and license is a MUST.</li>
                </ul>
                <br>
                <sub><i>send us your resume at: </i><b>info@primeopusinc.com</b></sub>
            </div>
            <div class="modal-footer">
                <sub class="mr-auto"><i>Send us your resume at: </i><b>info@primeopusinc.com</b></sub>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>