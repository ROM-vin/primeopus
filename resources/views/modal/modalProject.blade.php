<!-- project -->
<div class="modal fade project1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick">
                <div class="serviceModal">
                    <img data-src="img/project1.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">FLYNN BURNER</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project2-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project2.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">AFLX SULZER SUBMERSIBLE MIXED FLOW</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project3-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project3.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">SULZER APP CENTRIFUGAL PUMP</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project4-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project4.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">NETZSCH PROGRESSIVE CAVITY PUMP</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project5-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project5.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">NETZSCH DOSING PUMPS</p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal fade project6-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img src="{{asset('img/project6.jpg')}}" class="w-100" alt="">
                    <p class="text-center">NETZSCH IN ILUGIN</p>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="modal fade project7-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project7.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">NETZSCH PUMP IN WASTE WATER TREATMENT PLANT</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project8-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project8.jpg" class="w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">NETZSCH IN SUCERE</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project9-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project9.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">NETZSCH FSIP</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project10-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project10.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">NETZSCH DOSING PUMP</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project11-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project11.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center text-uppercase">Netzsch Sludge Dosing Pump</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project12-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-75 mx-auto">
                <div class="serviceModal">
                    <img data-src="img/project12.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">SULZER PUMPS IN WASTE WATER TREATMENT PLANT</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project13-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project13.jpg" class="w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">SULZER MIXER AT PASAY WATER TREATMENT PLANT </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <div class="modal fade project14-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img src="{{asset('img/project14.jpg')}}" class="w-100" alt="">
                    <p class="text-center">SULZER MIXER AT PNB PASAY</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project15-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img src="{{asset('img/project15.jpg')}}" class="w-100" alt="">
                    <p class="text-center">SULZER MIXER IN PASAY WTP MAYNILAD</p>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="modal fade project16-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project16.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center text-uppercase">Sulzer Pumps at Power Plant</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade project17-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-1">
            <div class="service-slick w-100">
                <div class="serviceModal">
                    <img data-src="img/project17.jpg" class="lazy w-100" style="object-fit: contain;" alt="">
                    <p class="text-center">SULZER XFP PUMPS</p>
                </div>
            </div>
        </div>
    </div>
</div>