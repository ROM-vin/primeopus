<!-- services -->
<!-- Submersible Pumps -->
<div class="modal fade service58-modal-lg" id="service1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-2">
            <div class="service-modal w-75 mx-auto" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/submersible-v1.jpg" class="lazy w-50 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-2.jpg" class="lazy w-100 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-3.jpg" class="lazy w-100 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-4.jpg" class="lazy w-100 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-5.jpg" class="lazy w-100 mx-auto" alt=""></div>
                <!-- <div class="serviceModal"><img data-src="img/submersible-6.jpg" class="lazy w-50 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-7.jpg" class="lazy w-50 mx-auto" alt=""></div> -->
                <div class="serviceModal"><img data-src="img/submersible-8.jpg" class="lazy w-50 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-9.jpg" class="lazy w-50 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-10.jpg" class="lazy w-100 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-11.jpg" class="lazy w-100 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-12.jpg" class="lazy w-100 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/submersible-13.jpg" class="lazy w-100 mx-auto" alt=""></div>
            </div>
        </div>
    </div>
</div>

<!-- Centrifugal Pump -->
<div class="modal fade service1-modal-lg" id="service2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/centrifugal-1.jpg" class="lazy w-100"  alt=""></div>
                <div class="serviceModal"><img data-src="img/centrifugal-2.jpg" class="lazy w-100"  alt=""></div>
                <div class="serviceModal"><img data-src="img/centrifugal-3.jpg" class="lazy w-100"  alt=""></div>
                <div class="serviceModal"><img data-src="img/centrifugal-4.jpg" class="lazy w-100"  alt=""></div>
                <div class="serviceModal"><img data-src="img/centrifugal-5.jpg" class="lazy w-100"  alt=""></div>
                <div class="serviceModal"><img data-src="img/centrifugal-6.jpg" class="lazy w-50 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/centrifugal-7.jpg" class="lazy w-100" alt=""></div>
            </div>
        </div>
    </div>
</div>

<!-- Dewatering Pumps -->
<div class="modal fade service8-modal-lg" id="service3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/dewatering-1.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-4.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-5.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-6.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-7.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-8.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-9.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-10.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-11.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/dewatering-12.jpg" class="lazy w-100" alt=""></div>
            </div>
        </div>
    </div>
</div>

<!-- Submersible Mixer -->
<div class="modal fade service20-modal-lg" id="service4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/mixer-1st-1.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-3.jpg" class="lazy w-75 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-1.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-4.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-5.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-6.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-4.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-5.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-6.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-7.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-8.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-9.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-7.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-8.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-2nd-9.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-10.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/mixer-1st-11.jpg" class="lazy w-100" alt=""></div>
            </div>
        </div>
    </div>
</div>

<!-- Progressive Cavity Pump -->
<div class="modal fade service41-modal-lg" id="service5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/progressive-cavity-1.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-4.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-5.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-6.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-7.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-8.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-9.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-10.jpg" class="lazy w-50 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/progressive-cavity-11.jpg" class="lazy w-100" alt=""></div>
                <!-- <div class="serviceModal"><img data-src="img/progressive-cavity-12.jpg" class="lazy w-100" alt=""></div> -->
            </div>
        </div>
    </div>
</div>

<!-- Rebonding of V-Rope Guide -->
<div class="modal fade service52-modal-lg" id="service6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/rebonding-1.jpg" class="lazy w-100" alt=""></div>
                <!-- <div class="serviceModal"><img data-src="img/rebonding-2.jpg" class="lazy w-100" alt=""></div> -->
                <div class="serviceModal"><img data-src="img/rebonding-3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/rebonding-4.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/rebonding-5.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/rebonding-6.jpg" class="lazy w-100" alt=""></div>
            </div>
        </div>
    </div>
</div>

<!-- site equipment check up and inspection -->

<div class="modal fade service72-modal-lg" id="service7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/checkup1.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup4.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup5.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup6.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup7.jpg" class="lazy w-75 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup8.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup10.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup11.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup14.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/checkup15.jpg" class="lazy w-100" alt=""></div>
            </div>
        </div>
    </div>
</div>
<!-- testing and commissioning -->
<div class="modal fade" id="service8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/commision1.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision7.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision9.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision11.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision12.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision14.jpg" class="lazy w-100" alt=""></div><!-- 
                <div class="serviceModal"><img data-src="img/commision15.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision16.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision17.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision18.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision19.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision20.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision21.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/commision22.jpg" class="lazy w-100" alt=""></div> -->
            </div>
        </div>
    </div>
</div>


<!-- installation service -->
<div class="modal fade" id="installation1" tabindex="1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/installation2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.1.jpg" class="lazy mx-auto w-75" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.3.jpg" class="lazy mx-auto w-75" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.4.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.5.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.6.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.7.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.8.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.9.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.10.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation2.12.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation3.2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation3.3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation5.2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation5.6.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation5.7.jpg" class="lazy w-100" alt=""></div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="installation2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100 mx-auto" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/installation1.3.jpg" class="lazy w-75 mx-auto" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation1.4.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation1.6.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation1.8.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation1.9.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation1.10.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation1.11.jpg" class="lazy w-100" alt=""></div>                
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="installation3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content p-5">
            <div class="service-modal w-100 mx-auto" style="max-height:75vh">
                <div class="serviceModal"><img data-src="img/installation4.1.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation4.2.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation4.3.jpg" class="lazy w-100" alt=""></div>
                <div class="serviceModal"><img data-src="img/installation4.4.jpg" class="lazy w-50 mx-auto" alt=""></div>
            </div>
        </div>
    </div>
</div>

