<div class="container">

    <div class="row">
        <div class="col">
            <h2 class="sectionHeader-h2 headerColor">Welcome to Prime&nbsp;Opus&nbsp;Inc.</h2>
        </div>
    </div>
    <div class="row px-5 justify-content-center text-justify">
        <div class="col-md-12">
            <p class="landingText mb-3">Prime Opus Inc. is engaged in the supply of parts and equipment originating in Europe, Japan and U.S.A. for the food and beverage, packaging, pulp and paper, power generating plants, water and waste water treatment plant industries.</p>
        </div>
    </div>
    <div class="row px-5 mt-5 justify-content-center text-justify">
        <div class="col-md-6">
            <h1 class="text-center">Mission</h1>
            <p class="landingText text-justify">To provide our clients with the best valued quality industrial equipment, excellent after-sales service and deliver correct products as required and to be the number one sales organization for every company we serve.</p>
        </div>
        <div class="col-md-6 ">
            <h1 class="text-center">Vision</h1>
            <p class="landingText text-justify">Prime Opus Inc. will be the preferred supplier for industrial equipment in the Food & Beverages, Pulp & Paper, Cement, Energy & Environment, Sugar, Water and Wastewater Industries through a reputation of professionalism, integrity and significant contribution to the growth of our clients.</p>
        </div>
    </div>
    <hr class="p-2">
<!-- PRODUCTS -->

    <div class="row" id="product">
        <div class="col">
            <h3 class="sectionHeader-h3 pt-5 text-center text-uppercase"><u>&nbsp;products&nbsp;</u></h3>    
        </div>
    </div>
    <div class="row px-5  justify-content-center" >   
       
        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-placement="top" title="click to view" data-target=".bd-Netzsch-modal-xl">
                <img data-src="img/netzsch.png" class="lazy card-img-top py-3 w-100"  alt="">
            </button>
            @include('products.netzschProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Addinol-modal-xl">
                <img data-src="img/ADDINOL-Logo.png" class="lazy card-img-top py-3 w-100"  alt="">
            </button>
            @include('products.addinolProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Flynn-modal-xl">
                <img data-src="img/flynn.png" class="lazy card-img-top py-3 w-100"  alt="">
            </button>
            @include('products.flynnProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Wirebelt-modal-xl">
                <img data-src="img/wirebelt.png" class="lazy card-img-top py-3 w-100"  alt="">
            </button>
            @include('products.wirebeltProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Sulzer-modal-xl">
                <img data-src="img/sulzer.png" class="lazy card-img-top py-3 w-100"  alt="">
            </button>
            @include('products.sulzerProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Kongskilde-modal-xl">
                <img data-src="img/kongskilde.png" class="lazy card-img-top py-3 w-100" alt="">
            </button>
            @include('products.kongskildeProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Ipco-modal-xl">
                <img data-src="img/ipco.png" class="lazy card-img-top py-3 w-100" alt="">
            </button>
            @include('products.ipcoProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Scanbelt-modal-xl">
                <img src="{{asset('img/scanbelt.png')}}" class="lazy card-img-top py-3 w-100" alt="">
            </button>
            @include('products.scanbeltProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Macintyre-modal-xl">
                <img data-src="img/macintyre1.png" class="lazy card-img-top py-3 w-100" alt="">
            </button>
            @include('products.macintyreProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-Haug-modal-xl">
                <img data-src="img/HAUG-Logo.png" class="lazy card-img-top py-3 w-100" alt="">
            </button>
            @include('products.haugProduct')
        </div>

        <div class="col-md-3">
            <button type="button" class="btn btn-link" data-toggle="modal" data-target=".bd-SAS-modal-xl">
                <img data-src="img/sas.png" class="lazy card-img-top py-3 w-100" alt="">
            </button>
            @include('products.sasProduct')
        </div>
        

    <!-- <div class="row pb-3 ">
        <div class="col px-5">
            <a href="{{route('viewAll')}}"><button class="btn btn-prime float-right animated fadeInRight">view all</button></a>
        </div>
    </div> -->
    </div>
</div>
<br>
<!-- KEY ACCOUNTS -->
<div class="container-fluid py-4">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3 class="sectionHeader-h3 text-center text-uppercase"><u>&nbsp;Key&nbsp;Accounts&nbsp;</u></h3>
            </div>
        </div>
        <div class="row justify-content-center mx-auto">
            <div class="col-md-3 text-center p-1"><img class="lazy   rounded-lg w-50" data-src="img/Maynilad.png" alt=""></div>
            <!-- <div class="col-md-3 text-center  p-1"><img class="lazy   rounded-lg w-50" src="{{asset('img/SCG.png')}}" alt=""></div> -->
            <div class="col-md-3 text-center p-1"><img class="lazy   rounded-lg w-50" data-src="img/SIEMENS.png"  alt=""></div>
            <div class="col-md-3 text-center p-1"><img class="lazy   rounded-lg w-50" data-src="img/URC-Universal_Robina_Corp.png" alt=""></div>
            <div class="col-md-3 text-center p-1"><img class="lazy   rounded-lg w-50" data-src="img/Manila_Water.png" alt=""></div>
            <div class="col-md-3 text-center p-1"><img class="lazy   rounded-lg w-50" data-src="img/san_miguel_corp.png" alt=""></div>
            <div class="col-md-3 text-center p-1"><img class="lazy   rounded-lg w-50" data-src="img/REBISCO.png" alt=""></div>
            <div class="col-md-3 text-center p-1"><img class="lazy   rounded-lg w-50" data-src="img/JGS_Petro_chem.png" alt=""></div>
        </div>
    </div>

</div>





<!-- SERVICES -->
<div class="container">
    <div class="col" id="services">
        <h3 class="sectionHeader-h3 font-weight-bolder text-center pt-5">services & projects</h3>
    </div>

    <div class="row px-5">
        <div class="col">
            <p class="landingText px-3 text-justify">We specialize in providing quality and tested equipment that is worth investing for use throughout the Water and Wastewater Industries, Food & power generation industry and other industries. We are proficient not only when it comes to supplying our equipment, but also in providing after sales support to all our clients.</p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="{{route('projects')}}"><button class="btn btn-prime float-right animated fadeInRight mx-1">view all projects</button></a>
            <a href="{{route('services')}}"><button class="btn btn-prime float-right animated fadeInRight mx-1">view all services</button></a>
        </div>

    </div>
</div>
