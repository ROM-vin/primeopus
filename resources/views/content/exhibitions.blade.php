<div class="container mb-3 mt-3 pb-1">
<h3 class="sectionHeader-h3 text-center pt-5" id="Exhibits">News & Events</h3>


    <div class="row multiple-items px-5 justify-content-center">
  
        <div class="card m-3" style="width: 100%;">
            <img class="lazy card-img-top" data-src="{{asset('img/expo1.jpg')}}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Afex 2019 Asia Food Expo</h5>
                <a href="{{route('afex2019')}}" class="mb-3 btn btn-primary float-right">View</a>
            </div>
        </div>
   
    </div>


</div>

