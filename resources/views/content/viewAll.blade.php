<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('img/prime-logo.png')}}" type="image/x-icon">
    <title>Prime Opus Inc.</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="nav">
    <a class="navbar-brand" href="{{route('index')}}"><img src="{{asset('img/prime-name.png')}}" alt="Navbar"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('index')}}">home</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="row text-center">
        <div class="col">
            <img src="{{asset('img/Under_Construction.png')}}" width="70%" alt="">
            <br>
            <h2 class="pt-5" style="font-family:'Times New Roman', Times, serif;">Under Maintenance</h2>
        </div>
    </div>
</div>

    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/easing.jquery.js')}}"></script>
    <script src="{{asset('js/scrolling-nav.js')}}"></script>
</body>
</html>