<div id="topCarousel" class="carousel slide carousel-fade" data-ride="carousel">

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{asset('/img/1st_home.jpg')}}" alt="wirebelt">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="line-1 rounded-lg">quality service at hand</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset('/img/2nd_home.jpg')}}" alt="sulzer">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="line-1 rounded-lg">quality service at hand</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{asset('/img/3rd_home.jpg')}}" alt="kongskilde">
      <div class="carousel-caption d-none d-md-block">
        <h1 class="line-1 rounded-lg">quality service at hand</h1>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#topCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#topCarousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>