<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">

    <!-- Slick-Carousel -->
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>


    {{-- swal --}}
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">

    <link rel="shortcut icon" href="{{asset('img/prime-logo.png')}}" type="image/x-icon">
    <title>Prime Opus Inc.</title>
</head>
<body>
    @include('partial.navigation')
    @yield('content')
    @include('partial.footer')


    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/jquery.lazy.min.js')}}"></script>
    <script src="{{asset('js/easing.jquery.js')}}"></script>
    <script src="{{asset('js/scrolling-nav.js')}}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@17.1.0/dist/lazyload.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        $(document).ready(function(){

            var pathname = window.location.pathname;
            if(pathname == '/'){
                $('#servicePage').hide();
                $('#projectPage').hide();
            }

            if(pathname == '/services'){
                $('#Navproduct').hide();
                $('#Navservices').hide();
                $('#Navexhibits').hide();
                $('#Navcareers').hide();
                $('#Navcontact').hide();
                $('#servicePage').show();
                $('#projectPage').hide();
            }
            if(pathname == '/projects'){
                $('#Navproduct').hide();
                $('#Navservices').hide();
                $('#Navexhibits').hide();
                $('#Navcareers').hide();
                $('#Navcontact').hide();
                $('#servicePage').hide();
                $('#projectPage').show();
            }
            if(pathname == '/news'){
                $('#Navproduct').hide();
                $('#Navservices').hide();
                $('#Navexhibits').show();
                $('#Navcareers').hide();
                $('#Navcontact').hide();
                $('#servicePage').hide();
                $('#projectPage').hide();
            }
            // careers slick carousel
            $('.clientCarousel').slick({
                arrows:true,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2500,
                    responsive: [
                        {
                            breakpoint: 1024,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 3,
                                    infinite: true,
                                }
                        },
                        {
                            breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                        },
                        {
                            breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                        }
                    ]
            });
            // news slick carousel
            $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
            
            });
            $('.slider-nav').slick({
            arrows: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            focusOnSelect: true,
            centerMode: true,
            centerPadding: '5px',
            autoplay:true,      
                responsive:[
                    {
                        breakpoint: 768,
                            settings:{
                                vertical:false,
                                verticalSwiping:false,
                                slidesToShow: 3,
                            }
                    }
                ]

            });


            // services slick
            $('.service-modal').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                fade: true,
                asNavFor: '.service-nav'
            });
            $('.service-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.service-modal',
                dots: false,
                centerMode: false,
                infinite: true,
                focusOnSelect: true,
                    responsive: [
                        {
                            breakpoint: 1024,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 3,
                                    infinite: true,
                                }
                        },
                        {
                            breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                        },
                        {
                            breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    autoplay: true,
                                    arrows: true,
                                }
                        }
                    ]
            });
            $('.multiple-items').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: false,
                    responsive: [
                        {
                            breakpoint: 1024,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 3,
                                    infinite: true,
                                }
                        },
                        {
                            breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                        },
                        {
                            breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    autoplay: true,
                                    arrows: false,
                                }
                        }
                    ]
            });

            $(function() {
                $(".lazy").lazy({effect : "fadeIn"});
            });

            $(document).ready(function(){
                if(window.matchMedia('(max-width: 500px)').matches){
                    $('.mobileView').removeClass('col-md-2');
                    $('.mobileView').addClass('col-6');
                    
                }
            });
        });
        // readmore
        $(document).ready(function() {
          $("#toggle").click(function() {
            var elem = $("#toggle").text();
            if (elem == "Read More") {
              //Stuff to do when btn is in the read more state
              $("#toggle").text("Read Less");
              $("#text").slideDown();
            } else {
              //Stuff to do when btn is in the read less state
              $("#toggle").text("Read More");
              $("#text").slideUp();
            }
          });
        });

        // Mail

        $("#btnSend").click(function(e){
            e.preventDefault();
            var email = $("#eMail");
            var name = $("#contactName");
            var number = $("#contactNumber");
            var message = $("#Message");
            if(name.val() == "" && email.val() == "" && number.val() == "" && message.val() == ""){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',
                    })
            }else{
                $.ajax({
                    url: '/send',
                    method : 'POST',
                    data : {
                        '_token' : '{{ csrf_token() }}',
                        'name' : name.val(),
                        'message' : message.val(),
                        'number' : number.val(), 
                        'email' : email.val()
                    },
                    success : function(data){
                        if(data == 1){
                                Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Message Sent',
                                showConfirmButton: false,
                                timer: 1500
                                })
                            name.val("");
                            email.val("");
                            number.val("");
                            message.val("");
                        }
                    },
                    beforeSend : function(data){
                        Swal.fire({
                                position: 'center',
                                width: 400,
                                title: 'Sending Your Email',
                                imageUrl: 'https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif',
                                imageWidth: 200,
                                imageHeight: 200,
                                showConfirmButton: false,
                                timer: 2000
                        })
                    }
                });
            }
            
        });
    </script>
</body>
</html>