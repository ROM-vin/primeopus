<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('partial.main');
})->name('index');

Route::get('/viewAll', function () {
    return view('content.viewAll');
})->name('viewAll');

Route::get('/news', function () {
    return view('news.afex2019');
})->name('afex2019');

Route::get('/services', function (){
    return view('partial.services');
})->name('services');
Auth::routes();

Route::get('/projects', function (){
    return view('partial.projects');
})->name('projects');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/send', function(){
    Mail::to('info@primeopusinc.com')->send(new \App\Mail\Contact($contact));
});

Route::post('/send', 'ContactFormContoller@sendMail');